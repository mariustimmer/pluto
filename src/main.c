#include <libintl.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <pluto_runtime.h>

/**
 * Main routine which will be called by the operating
 * system and which invokes the whole Pluto thing.
 * @param argc Number of arguments given by the command line
 * @param argv Arguments given by the call from the command line
 * @return Exit status - EXIT_SUCCESS or EXIT_FAILURE
 */
int main(
	int	 argc,
	char	*argv[]
) {
	bool startup, shutdown;
	startup = pluto_startup();
	if (!startup) {
		fprintf(
			stderr,
			"Startup failed\n"
		);
		return EXIT_FAILURE;
	}
	pluto_main();
	shutdown = pluto_shutdown();
	if (!shutdown) {
		fprintf(
			stderr,
			"%s\n",
			gettext("Error during shutdown")
		);
	}
	return EXIT_SUCCESS;
}
