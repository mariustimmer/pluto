#include <errno.h>
#include <libintl.h>
#include <locale.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pluto_runtime.h>

#define LOCALE_PACKAGE_NAME	"pluto"
#define LOCALE_DIRECTORY	"locale"
#define LOCALE_NAME			""

bool pluto_running;

/**
 * @var pluto_cycle Number of cycles within the pluto_main() method.
 */
unsigned long pluto_cycle;

int pluto_cycle_interval = 5;

bool pluto_startup(
) {
	// Set up locale
	if (setlocale(LC_ALL, LOCALE_NAME) == NULL) {
		fprintf(
			stderr,
			"Could not set locale according to your environment\n"
		);
		return false;
	}
	if (bindtextdomain(LOCALE_PACKAGE_NAME, LOCALE_DIRECTORY) == NULL) {
		fprintf(
			stderr,
			"%s\n",
			gettext("Could not bind text domain")
		);
		return false;
	}
	if (textdomain(LOCALE_PACKAGE_NAME) == NULL) {
		fprintf(
			stderr,
			"%s\n",
			gettext("Could not set text domain")
		);
		return false;
	}
	// Bind the signal catcher
	if (signal(SIGINT, pluto_signal_catcher) == SIG_ERR) {
		fprintf(
			stderr,
			"%s\n",
			gettext("Could not add signal catcher for SIGINT")
		);
		return false;
	}
	return true;
}

void pluto_signal_catcher(int signum) {
	fprintf(stdout, "%s: %d\n", gettext("Got signal"), signum);
	pluto_set_stop();
}

void pluto_main(
) {
	pluto_running = true;
	pluto_cycle   = 0;
	while (pluto_running) {
		pluto_cycle++;
		if (sleep(pluto_cycle_interval) == 0) {
			// Thread could not sleep
			fprintf(stderr, "Could not sleep\n");
			pluto_set_stop();
		}
	}
	fprintf(stdout, "%s: %lu\n", gettext("Amount of executed cycles"), pluto_cycle);
}

/**
 * Set the running variable to false so there won't be a next loop.
 * This method does not stop the runtime but it will avoid the next execution.
 */
void pluto_set_stop() {
	pluto_running = false;
}

bool pluto_shutdown(
) {
	return true;
}
