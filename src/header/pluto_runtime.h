#ifndef HEADER_PLUTO_RUNTIME_H
#define HEADER_PLUTO_RUNTIME_H

#define PLUTO_SUCCESS

bool pluto_startup();
void pluto_signal_catcher(int signum);
void pluto_main();
void pluto_set_stop();
bool pluto_shutdown();

#endif
