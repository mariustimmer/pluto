CC=gcc
MKDIR=mkdir
RM=rm

DIRECTORY_SOURCE=./src
DIRECTORY_HEADER=$(DIRECTORY_SOURCE)/header
DIRECTORY_BUILD=./build
DIRECTORY_DIST=./dist
DIRECTORY_LOCALE=./locale

build: $(DIRECTORY_SOURCE)/main.c $(DIRECTORY_SOURCE)/pluto_runtime.c
	$(MKDIR) \
		-p \
		$(DIRECTORY_BUILD)
	$(CC) \
		-Wall \
		-I$(DIRECTORY_HEADER) \
		-o$(DIRECTORY_BUILD)/pluto \
		$(DIRECTORY_SOURCE)/main.c $(DIRECTORY_SOURCE)/pluto_runtime.c

i18n:
	msgfmt \
		--output=${DIRECTORY_LOCALE}/de_DE/pluto.mo \
		"${DIRECTORY_LOCALE}/de_DE/pluto.po"

clean: $(DIRECTORY_BUILD)/pluto
	$(RM) \
		-rf \
		$(DIRECTORY_BUILD) \
		$(DIRECTORY_DIST)

all:	clean build i18n

