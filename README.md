PLUTO
=====

This project aims to provide you a very useful personal assistant. It is not and will never be a real AI but since this is just another buzzword - who want's AI for real?

About
-----
Pluto is a UNIX daemon which will run in the background of your computer or server. It will be capable of tracking your location and appointments, may remind you using a messanger API (_like Telegram_) and you can interact with it using this API or the CLI.

State of the project
--------------------
Currently the project is about to start. Right now it is just an "runtime" which is capable of starting and quitting. The fact that there is only me developing the project means it will take it's time to be finished - but let's face it: It will never be "_finished_".

Contribute
----------
Have a look in the [contribution file](CONTRIBUTING.md).

License
-------
Have a look in the [license file](LICENSE.md).
