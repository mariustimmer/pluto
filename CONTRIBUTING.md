CONTRIBUTE
==========
Currently there is only me (_Marius Timmer_) maintaining the project. This may change in the future. Since I do not expect many contributions from others I will accept contributions by using the git mechanism (_fork the project and send me a change request_), E-Mail (_like real kernel developers_) or even by talking/chatting with me.
